﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKCOM_Record_Tool
{
    public class ToolObject
    {
		/// <summary>
		/// 根目錄路徑
		/// </summary>
		public string Root_Folder_Path { get; set; }

		public void Add_Log(string file_name, string file_content)
		{
			string Date = DateTime.Now.ToString("【yyyy-MM-dd】");
			string Now_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff");
			try
			{
				TextWriter tw = new StreamWriter(Root_Folder_Path + @"\output\log\" + Date + file_name + ".txt", true);
				tw.WriteLine(Now_Time);
				tw.WriteLine(file_content);
				tw.Close();
			}
			catch (Exception ex)
			{
				//string Now_Time_2 = DateTime.Now.ToString("yyyyMMdd HHmmssffffff");
				//TextWriter tw = new StreamWriter(Root_Folder_Path + @"\output\log\" + Date + "txt_error -" + Now_Time_2 + ".txt", true);
				//tw.WriteLine(Now_Time);
				//tw.WriteLine(ex.Message);
				//tw.Close();
			}
		}

		public void Check_Folder_Exist(string folder_name)
		{
			if (Directory.Exists(@Root_Folder_Path + folder_name) == false)
			{
				Directory.CreateDirectory(@Root_Folder_Path + folder_name);
			}
		}

		public void Open_File_Folder(string file_path)
		{
			System.Diagnostics.Process.Start("Explorer.exe", file_path);
		}

		public decimal Stock_Price_Calculate(decimal original_price, string calculate_type, int count)
		{
			decimal temp_price = original_price;
			int temp_count = count;
			switch (calculate_type)
			{
				case "+":
					while (count > 0)
					{
						temp_price = Stock_Price_Add_One_Degree(temp_price);
						count--;
					}
					break;
				case "-":
					while (count > 0)
					{
						temp_price = Stock_Price_Subtract_One_Degree(temp_price);
						count--;
					}
					break;
				default:
					break;
			}
			return temp_price;
		}

		public decimal Stock_Price_Add_One_Degree(decimal original_price)
		{
			decimal temp_price = original_price;

			if (temp_price > 1000)
			{
				temp_price += Convert.ToDecimal(5);
			}
			else if (temp_price >= 500 && temp_price < 1000)
			{
				temp_price += Convert.ToDecimal(1);
			}
			else if (temp_price >= 100 && temp_price < 500)
			{
				temp_price += Convert.ToDecimal(0.5);
			}
			else if (temp_price >= 50 && temp_price < 100)
			{
				temp_price += Convert.ToDecimal(0.1);
			}
			else if (temp_price >= 10 && temp_price < 50)
			{
				temp_price += Convert.ToDecimal(0.05);
			}
			else if (temp_price < 10)
			{
				temp_price += Convert.ToDecimal(0.01);
			}

			return temp_price;
		}

		public decimal Stock_Price_Subtract_One_Degree(decimal original_price)
		{
			decimal temp_price = original_price;

			if (temp_price > 1000)
			{
				temp_price -= Convert.ToDecimal(5);
			}
			else if (temp_price > 500 && temp_price <= 1000)
			{
				temp_price -= Convert.ToDecimal(1);
			}
			else if (temp_price > 100 && temp_price <= 500)
			{
				temp_price -= Convert.ToDecimal(0.5);
			}
			else if (temp_price > 50 && temp_price <= 100)
			{
				temp_price -= Convert.ToDecimal(0.1);
			}
			else if (temp_price > 10 && temp_price <= 50)
			{
				temp_price -= Convert.ToDecimal(0.05);
			}
			else if (temp_price <= 10)
			{
				temp_price -= Convert.ToDecimal(0.01);
			}

			return temp_price;
		}

		public decimal Convert_Tick_Price_toDecimal(int original_price)
		{
			return Math.Round(Convert.ToDecimal(original_price) / 100, 2, MidpointRounding.AwayFromZero);
		}

		/// <summary>
		/// tick回傳交易時間資料轉換成時間字串格式
		/// </summary>
		/// <param name="date">日期</param>
		/// <param name="mhs">時分秒</param>
		/// <param name="mis">微秒</param>
		/// <returns>完整日期格式 yyyy-MM-dd hh:mm:ss.ffffff</returns>
		public string Tick_Convert_To_Datetime(int date, int mhs, int mis)
		{
			return date.ToString().Substring(0, 4) + "-" + date.ToString().Substring(4, 2) + "-" + date.ToString().Substring(6, 2) + " " + mhs.ToString().PadLeft(6, '0').Substring(0, 2) + ":" + mhs.ToString().PadLeft(6, '0').Substring(2, 2) + ":" + mhs.ToString().PadLeft(6, '0').Substring(4, 2) + "." + mis.ToString().PadLeft(6, '0');
		}

		public void Datatable_Insert_Xls(DataTable dataTable, string table_name, string model, bool open_folder)
		{
			//系統時間
			string gtm = DateTime.Now.ToString("yyyyMMddHHmmssff");
			//註冊授權
			Spire.License.LicenseProvider.SetLicenseKey("JKFIWPyWXbLKNOIBAHIMabhlPd00tFIF6rW6dFGnBCBVl9draunSqL5fVB2PhDQ2m4T6dJnlOzNqD81qYZ92gUhIMW0Z5cIm/HTIco176BqrYINLwGWxRmP76itKB5IISkueibbF3YTZOgVcg+tGD/wvb0KqzVQ4BZVMJqP01qHrV3q5sM3u7iITJsf9xWmFPYkbZLlblhpmDklrCJbBSc95bNSsg+pyXxF1kB8WiL1Xeptw8Fo2y0xnfjV/b0nng5g47nDn/XhaPyJvHFxRD19RsecZnB3gFl+XVwOjhvO18O58dZjwIYg4WUQLprg6gDo7mF8n/kdbpPBjIP7eXSMNqljJXXGu5rK0sfAfOAj4CBM8e7JaaB9dfrgqCWelAhcM+B5RzQPxg+PxLHsqJp2fi6+ly17V2ffBo6QQyjQFUoAKlfIiSDq0DNdsQdLW+pQyPRhp8esFNhEZOGoBZGbdpT/5IslXhg5TaI1o0svzY77Cd7zgvaD7+dpYfovVhGp6Jv8Et4C94W/B59lWPIbh9rs3wrRCsZpBoPMn/39vYLHsGY02ymxEwB1fGEdURSR7+8Jvntj5lfwafqUWgM9UwH4BJU5TADBMiohrKXxZV3O2XjdCa1gzpwTdlZo26TcS0/pdtHDZ95hcracB4AEIX/HoFSSIR7bUtccFjUW7uLWEBh2kpf425CLOkSEnOslAgqPwep6gxc4hWlZxWxn6A55AbcHDtA70sJkObbLPCZFr2uGMpw4vBUBLEEDLZLaPs3kuOGWuf9e8vsShN0AEiP0PDlkpFmUgU6tdMHEG35evCIS4/l0bI7IFoBiqj2h2H59b7ZNYvl+FZdxgMTxXFA/eEazg4Vj3ZW4wBUA371PJgiD9inb7LQJcMCdyJTf2JO71KZPDYAeGkGr2CrBbBBqjUzp55SDVtIch+1Vmnvl9nDmFXFncKOfn11pbStA872Wqut0chojxhvXWXUsuHxeNIBvmKJy2nXl8zFlNLIG+f/jlt10fI4VD83kGGlTam/YOvrkhc7XnPRDpNRNsz2SYNYIJTp1kHUGNJLmKhMIEFHy+wEuG7AawK3E35HSFvT+brILdg4oa5iKx+PHZ3mdRYMcrcv8i0UYMibtlG2tYHevtQCN55OxtleYYbOQtJuASe7jD/44vb5fPr4zLMx0gJWDDb9DEy1LjBdfkzibaMr4/NQHwgcrzHcrHXL7oRffj5bmopDE4FFVWc5jw+znmZUoxKJaJx4CInqtrefdXACS7acBjj5RfWztNxUKFhZAocXylZ0/fzb/vo/JZl2oEnx6m+gTWRRsmHaXubnJVhuYfY4DLPFh6GCsUxje/EfsSoAmNWov2tAaZlUV1Q3BpQarQI5JB3z7JVZknIvA8rDP2JrZug9waPmvYXw35eddg+20KzHFv9/obGwx0SPV4fX5BLDk0sG3j/ZuChfqja5SicqdTrj8NDfHONRxJN3gqTxWirH3Vm3FaqA==");
			//宣告spire 物件
			Workbook book = new Workbook();

			if (model != "none")
			{
				//有指定樣式就不寫入第一行欄位名稱
				book.LoadTemplateFromFile(@Root_Folder_Path + @"\output\Excel_Templata\" + model + ".xlsx");
				Spire.Xls.Worksheet sheet = book.Worksheets[0];
				sheet.InsertDataTable(dataTable, false, 2, 1);
				sheet.AllocatedRange.AutoFitColumns();
			}
			else
			{
				//沒指定樣式第一行預設為欄位名稱
				//宣告寫入分頁
				Worksheet sheet = book.Worksheets[0];
				//資料表寫入分頁
				sheet.InsertDataTable(dataTable, true, 1, 1);
				sheet.AllocatedRange.AutoFitColumns();
			}

			//儲存檔案
			book.SaveToFile(@Root_Folder_Path + @"\output\Relesae\" + table_name + ".xlsx");
			if (open_folder)
			{
				Open_File_Folder(@Root_Folder_Path + @"\output\Relesae\");
			}
		}

	}
}
