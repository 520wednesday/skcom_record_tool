﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKCOM_Record_Tool
{
    public class SQLObject
    {
        #region Environment Variable
        //----------------------------------------------------------------------
        // Environment Variable
        //----------------------------------------------------------------------


        /// <summary>
        /// SQL Server名稱
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DataBase { get; set; }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// 使用者密碼
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 連線逾時等待秒數
        /// </summary>
        public string Timeout { get; set; } = "10";

        /// <summary>
        /// Log存放路徑
        /// </summary>
        public string LogPath { get; set; } = "";

        /// <summary>
        /// 是否建立Log資訊，預設無(false)
        /// </summary>
        public bool GenerateLog { get; set; } = false;

        #endregion

        /// <summary>
        /// 建立連線字串
        /// </summary>
        /// <returns>連線資訊</returns>
        public string connectionString()
        {
            return @"server=" + Server + @";database=" + DataBase + ";uid=" + User + ";pwd=" + Password + ";Connect Timeout = " + Timeout;
        }

        /// <summary>
        /// SQL連線測試，連線成功則傳回空字串，連線失敗傳回失敗原因
        /// </summary>
        /// <returns>測試結果</returns>
        public string ConnectingTest()
        {
            try
            {
                SqlConnection con = new SqlConnection();
                SqlCommand com = new SqlCommand();
                con.ConnectionString = connectionString();
                con.Open();
                con.Close();
            }
            catch (Exception ex)
            {
                Add_file_Log("SQL_Connecting_Test_err", ex.Message);
                Add_SQL_Log("SQL-Connecting_Test err", "ex-msg = " + ex.Message);
                return ex.Message;
                throw;
            }
            return "";
        }

        /// <summary>
        /// 設定SQL連接資訊
        /// </summary>
        public void SetConnectingValue(string server, string database, string username, string password, string timeout)
        {
            Server = server;
            DataBase = database;
            User = username;
            Password = password;
            Timeout = timeout;
        }

        /// <summary>
        /// 執行SQL查詢命令
        /// </summary>
        /// <param name="sqlquery">SQL命令</param>
        /// <returns>搜尋結果</returns>
        public DataTable Search(string sqlquery)
        {
            DataTable ds = new DataTable();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = connectionString();
            con.Open();
            try
            {
                using (con)
                {
                    using (SqlCommand cmd = new SqlCommand(sqlquery))
                    {
                        cmd.Connection = con;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(ds);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Add_SQL_Log("SQL-Search err", "sqlquery = " + Environment.NewLine + sqlquery + Environment.NewLine + "ex-msg = " + ex.Message);
            }
            con.Close();
            return ds;
        }

        /// <summary>
        /// 執行SQL命令
        /// </summary>
        /// <param name="sqlquery">SQL命令</param>
        public bool Execute(string sqlquery)
        {
            SqlDataReader dr;
            SqlCommand com = new SqlCommand();
            SqlConnection con = new SqlConnection();
            try
            {
                con.ConnectionString = connectionString();
                con.Open();
                com.Connection = con;
                com.CommandText = sqlquery;
                dr = com.ExecuteReader();
            }
            catch (Exception ex)
            {
                Add_SQL_Log("SQL-Execute err", "sqlquery = " + Environment.NewLine + sqlquery + Environment.NewLine + "ex-msg = " + ex.Message);
                return false;
            }
            con.Close();
            return true;
        }

        /// <summary>
        /// 執行SQL命令並回傳執行結果
        /// </summary>
        /// <param name="sqlquery">SQL命令</param>
        /// <returns>執行結果</returns>
        public string GetResult(string sqlquery)
        {
            string result = "";
            SqlCommand com = new SqlCommand();
            SqlConnection con = new SqlConnection();
            try
            {
                con.ConnectionString = connectionString();
                con.Open();
                com.Connection = con;
                com.CommandText = sqlquery;
                result = com.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                Add_SQL_Log("SQL-GetResult err", "sqlquery = " + Environment.NewLine + sqlquery + Environment.NewLine + "ex-msg = " + ex.Message);
            }
            con.Close();
            return result;
        }

        /// <summary>
        /// 建立Log訊息
        /// </summary>
        /// <param name="filename">檔案名稱</param>
        /// <param name="content">Log訊息</param>
        private void Add_file_Log(string filename, string content)
        {
            string date = DateTime.Now.ToString("【yyyy-MM-dd】");
            string Now_Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff");
            try
            {
                if (GenerateLog && LogPath != "")
                {
                    string file_path = LogPath + @"\" + date + filename + ".txt";
                    TextWriter tw = new StreamWriter(file_path, true);
                    tw.WriteLine(Now_Time);
                    tw.WriteLine(content);
                    tw.Close();
                }

            }
            catch (Exception)
            {
                //Add_SQL_Log("SQL-GetResult err", "sqlquery = " + Environment.NewLine + sqlquery + Environment.NewLine + "ex-msg = " + ex.Message);
            }
        }

        public void Add_SQL_Log(string title, string content)
        {
            SqlDataReader dr;
            SqlCommand com = new SqlCommand();
            SqlConnection con = new SqlConnection();
            string datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff");
            string query = @"INSERT INTO [dbo].[err_log]
                                   ([err_time]
                                   ,[err_title]
                                   ,[err_msg])
                             VALUES
                                   ('" + datetime + @"'
                                   ,'" + title + @"'
                                   ,'" + content + @"')";
            try
            {
                con.ConnectionString = connectionString();
                con.Open();
                com.Connection = con;
                com.CommandText = query;
                dr = com.ExecuteReader();
            }
            catch (Exception ex)
            {
                string errMessage = "SQL Query =" + query + Environment.NewLine + "err-message = " + ex.Message;
                Add_file_Log("SQL_Execute-Err", errMessage);
            }
            con.Close();
        }
    }
}
