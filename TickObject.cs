﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKCOM_Record_Tool
{
    public class TickObject
    {
        /*API提供參數*/
        //報價有異動的商品市場別
        public short sMarketNo { get; set; }
        //系統所編的索引代碼
        public int nStockIdx { get; set; }
        //第幾筆成交明細, 由0開始
        public int nPtr { get; set; }
        //交易日(YYYYMMDD)
        public int nDate { get; set; }
        //時間(時分秒hh:mm:ss)
        public int nTimehms { get; set; }
        //時間(毫秒微秒“ms”μs)
        public int nTimemillismicros { get; set; }
        //買價
        public decimal nBid { get; set; }
        //賣價
        public decimal nAsk { get; set; }
        //成交價
        public decimal nClose { get; set; }
        //量
        public int nQty { get; set; }
        //揭示 0:一般 1:試算
        public int nSimulate { get; set; }

        /*客製變數*/
        public string tick_time { get; set; }
        public DateTime receive_time { get; set; }
        public SQLObject SQLOb { get; set; }
        public ToolObject ToolOb { get; set; }

        public StockObject StockOb { get; set; }

        public string stock_num { get; set; }
        /// <summary>
        /// 紀錄tick訊息
        /// </summary>
        public void Store_Tick()
        {
            string query = ""; //sql語法
            string gtm_now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff");
            string sqlquery = @"MERGE [dbo].[tick_" + nDate + @"] AS T
								USING (VALUES('" + nStockIdx + @"','" + nPtr + @"','" + nSimulate + @"','" + tick_time + @"','" + sMarketNo + @"','" + nDate + @"','" + nTimehms + @"','" + nTimemillismicros + @"','" + nBid + @"','" + nAsk + @"','" + nClose + @"','" + nQty + @"','" + StockOb.Stock_Num + @"'))
								AS S ([nStockIdx], [nPtr],[nSimulate],[tick_time],[sMarketNo],[nDate],[nTimehms],[nTimemillismicros],[nBid],[nAsk],[nClose],[nQty],[stock_num])
								ON (S.[nStockIdx] = T.[nStockIdx] AND S.[nPtr] = T.[nPtr] )
								WHEN MATCHED THEN 
								UPDATE SET [nSimulate] = S.[nSimulate]
								,[tick_time] = S.[tick_time]
								,[sMarketNo] = S.[sMarketNo]
								,[nDate] = S.[nDate]
								,[nTimehms] = S.[nTimehms]
								,[nTimemillismicros] = S.[nTimemillismicros]
								,[nBid] = S.[nBid]
                                ,[nAsk] = S.[nAsk]
								,[nClose] = S.[nClose]
								,[nQty] = S.[nQty]
                                ,[stock_num] = S.[stock_num]
								WHEN NOT MATCHED THEN    
								INSERT ([nStockIdx], [nPtr],[nSimulate],[tick_time],[sMarketNo],[nDate],[nTimehms],[nTimemillismicros],[nBid],[nAsk],[nClose],[nQty],[stock_num])
								VALUES (S.[nStockIdx], S.[nPtr],S.[nSimulate],S.[tick_time],S.[sMarketNo],S.[nDate],S.[nTimehms],S.[nTimemillismicros],S.[nBid],S.[nAsk],S.[nClose],S.[nQty],S.[stock_num]);";
            SQLOb.Execute(sqlquery);
        }

    }
}
